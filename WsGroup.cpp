#include "WsGroup.h"

WsGroup::WsGroup(std::string pPath){
  mPath = pPath;
  mConList.clear();
  mRx = new WsMessageQueue(10);
  mMax = 10;
}

std::string WsGroup::getPath(){
  return mPath;
}

void WsGroup::setMaxConnections(int pConnections){
  mMax = pConnections;
}

int WsGroup::addConnection(WsConnection *pConnection){
  cleanup();
  if(mConList.size() >= mMax){
    return -1;
  }
  if(pConnection->startRxThread(mRx) < 0){
    return -1;
  }
  mConList.push_back(pConnection);
  return 0;
}

int WsGroup::sendMessage(WsMessage *pMsg){
  unsigned int c;
  if(pMsg->getRoute() == WS_BROADCAST){
    for(c = 0; c < mConList.size(); c++){
      mConList[c]->sendMessage(pMsg);
    }
  }else{
    for(c = 0; c < mConList.size(); c++){
      if(mConList[c]->getUid() == pMsg->getRoute()){
	mConList[c]->sendMessage(pMsg);
	return 0;
      }
    }
  }
  return -1;  
}


WsMessage *WsGroup::getMessage(){
  return mRx->pop();
}

void WsGroup::cleanup(){
  WsConnection *wc;
  std::vector<WsConnection*>::iterator it;
  for(it = mConList.begin(); it != mConList.end(); it++){
    if((*it)->isRunning() == false){
      wc = *it;
      mConList.erase(it);
      delete wc;
      cleanup();
      return;
    }
  }
}
