#include <stdio.h>
#include "WsServer.h"
#include "WsConnection.h"
#include "WsMessage.h"
#include "WsGroup.h"

int main(){
  WsServer *ws;
  WsMessage *msg;
  WsMessage *txmsg;
  WsGroup *wg;
  ws = new WsServer();
  wg = new WsGroup("/echo");
  ws->insertGroup(wg);
  if(ws->startServer()<0){
    printf("failed to start server\n");
    return -1;
  }
  msg = wg->getMessage();
  if(msg == NULL){
    printf("failed to get message\n");
    return -1;
  }
  printf("Message: %s\n", msg->getData());
  txmsg = new WsMessage(129);
  txmsg->insertData(msg->getData(), msg->size());
  if(wg->sendMessage(txmsg) != 0){
    printf("Failed to write message\n");
  }
}
