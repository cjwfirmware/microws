#ifndef WS_GROUP_H
#define WS_GROUP_H


#include <string>
#include <vector>
#include <pthread.h>
#include "WsConnection.h"
#include "WsMessage.h"
#include "WsMessageQueue.h"

class WsGroup{
public:
  WsGroup(std::string pPath);
  std::string getPath();
  void setMaxConnections(int pConnections);
  int addConnection(WsConnection *pConnection);
  int sendMessage(WsMessage *pMsg);
  WsMessage *getMessage();
private:
  void cleanup();
  unsigned int mMax;
  std::string mPath;
  WsMessageQueue *mRx;
  std::vector<WsConnection*> mConList;
};
#endif
