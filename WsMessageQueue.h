#ifndef WS_MESSAGE_QUEUE_H
#define WS_MESSAGE_QUEUE_H

#include<stdio.h>
#include<semaphore.h>
#include<malloc.h>
#include"WsMessage.h"



class WsMessageQueue{
 public:
  WsMessageQueue(int pSize);
  int push(WsMessage *pRoot);
  WsMessage *pop();
  int count();
 private:
  int mStart;
  int mEnd;
  int mSize;
  int mCount;
  WsMessage **mQueue;
  sem_t mLock;
  sem_t mAvail;
};

#endif
