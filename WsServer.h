#ifndef WS_SERVER_H
#define WS_SERVER_H

#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <openssl/sha.h>
#include <string>
#include <vector>
#include <signal.h>
#include <pthread.h>
#include "WsConnection.h"
#include "WsHeader.h"
#include "WsRoute.h"
#include "WsGroup.h"

class WsServer{
 public:
  WsServer();
  ~WsServer();
  void insertGroup(WsGroup *pGroup);
  int startServer();
  void stopServer();
  bool serverActive();
  void __run();
 private:
  WsConnection *wsAccept();
  unsigned char remap64(unsigned char pC);
  void base64(unsigned char *pIn, unsigned char *pOut);
  int writeHandShake(int pCsock, const char *pSecKey);
  bool validPath(std::string pPath);
  int mSock;
  sockaddr_in mServAddr;
  bool mActive;
  WsConId mUidCnt;
  pthread_t mThread;
  std::map<std::string, WsGroup*> mGroupMap;
};


#endif
