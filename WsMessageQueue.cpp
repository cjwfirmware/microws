#include "WsMessageQueue.h"

WsMessageQueue::WsMessageQueue(int pSize){
  int c;
  mStart = 0;
  mEnd = 0;
  mSize = pSize;
  mCount = 0;
  mQueue = (WsMessage**)malloc(sizeof(WsMessage*) * pSize);
  for(c = 0; c < mSize; c++){
    mQueue[c] = NULL;
  }
  sem_init(&mLock, 0, 1);
  sem_init(&mAvail, 0, 0);  
}


int WsMessageQueue::push(WsMessage *pRoot){
  sem_wait(&mLock);
  if(mCount >= mSize){
    sem_post(&mLock);
    return -1;
  }
  mQueue[mEnd]= pRoot;
  mEnd++;
  mCount++;
  mEnd = mEnd % mSize;
  sem_post(&mLock);
  sem_post(&mAvail);
  return 0;
}

WsMessage *WsMessageQueue::pop(){
  WsMessage *ret;
  sem_wait(&mAvail);
  sem_wait(&mLock);
  if(mCount == 0){
    sem_post(&mLock);
    return NULL;
  }
  ret = mQueue[mStart];
  mStart++;
  mStart = mStart % mSize;
  mCount = mCount - 1;
  sem_post(&mLock);
  return ret;
}

int WsMessageQueue::count(){
  return mCount;
}
