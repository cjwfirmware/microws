#ifndef WS_CONNECTION_H
#define WS_CONNECTION_H

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <pthread.h>
#include <semaphore.h>
#include "WsMessage.h"
#include "WsHeader.h"
#include "WsRoute.h"
#include "WsMessageQueue.h"

class WsConnection{
 public:
  WsConnection(int pSock, WsHeader *pHeader, WsConId pUid);
  ~WsConnection();
  WsMessage *getMessage();
  WsConId getUid();
  int startRxThread(WsMessageQueue *pQueue);
  void stopRxThread();
  int sendMessage(WsMessage *pMsg);
  std::string getPath();
  void shutdown();
  void __run();
  bool isRunning();
 private:
  int getLength(unsigned char pLenght);
  WsHeader *mHeader;
  WsConId mUid;
  int mSock;
  sem_t mTxLock;
  int mConnected;
  bool mIsRunning;
  WsMessageQueue *mRxQueue;
  pthread_t mThread;
};


#endif
